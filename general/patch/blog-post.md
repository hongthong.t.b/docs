## Create a blog post for a patch release

Blog posts for patch releases are the responsibility of the Release Managers to create. This can be prepared while the release is being prepared, but should not be posted until the packages are released.

## Required information

* Version that is being released. We'll refer to this as AA-B-C
* Date the release is happening. We'll refer to this as YYYY-MM-DD

## Create a MR for the blog post

1. Clone the www repository and install dependencies

    ```shell
    $ git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
    $ cd www-gitlab-com
    $ bundle install
    ```
1. Create a new branch

    ```shell
    $ git checkout -b create-AA-B-C-post
    ```
1. Generate a new post

    ```shell
    $ bundle exec rake 'release:patch[AA.B.C]'
    ```
1. Edit the template, ensure the following fields get completed:
    * title
    * author
    * author_gitlab
    * author_twitter *Please use gitlab if you don't want to use your own Twitter account*
    * description
    * Fill in link to the monthly release post on the 22nd
    * Add links to merge requests for included fixes, along with a brief description
    * Update the **Upgrade barometer** section as appropriate
1. Add, commit, and push the new blog post

    ```shell
    $ git add source/releases/posts/YYYY-MM-DD-gitlab-AA-B-C-released.html.md
    $ git commit -m "Adding AA-B-C blog post"
    $ git push --set-upstream origin create-AA-B-C-post
    ```
1. Click the link in the `git push` output to bring up the new Merge Request page for your branch
1. Fill in the `Title`, `Description`, and `Milestone`, and assign to yourself.
1. Click the `Submit merge request` button
1. When the review environment is ready, read your blog post and ensure everything looks correct. Fix any errors.
1. Once packages are released, assign the merge request to someone with write access to [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) to merge.
