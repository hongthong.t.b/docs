# Force auto-deploy tagging

There could be situation, like https://gitlab.com/gitlab-org/release-tools/issues/351, where we cannot rely on release-tools to tag a new auto-deploy.

## Workaround

If a situation like this happens there's a way to force tagging a build.
We can tag a fake security release on dev, this will skip all the checks.

Step by step instructions:
1. Disable `Auto-deploy tagging` job on [pipeline schedules]
1. Modify the `Auto-deploy tagging` job - add `ENV` variable `SECURITY` set to `true` on [pipeline schedules]
1. **make sure canonical and dev are in sync** - check auto-deploy branch on both GitLab and omnibus
1. Manually kick off Auto-deploy tagging job from [pipeline schedules]
1. From now on only manual tagging works, as soon as we have a full green pipeline on auto-deploy branch we can restore the automated process
1. Modify the `Auto-deploy tagging` job - remove `ENV` variable `SECURITY` on [pipeline schedules]
1. Enable `Auto-deploy tagging` job on [pipeline schedules]


[pipeline schedules]: https://gitlab.com/gitlab-org/release-tools/pipeline_schedules 
